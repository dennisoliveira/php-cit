<?php
require_once __DIR__ . '/../vendor/autoload.php';

use App\Services\Ex3\GetFileExtension;

$files = [
    "music.mp4",
	"video.mov",
	"imagem.jpeg"
];

$getFileExtension = new GetFileExtension();
$orderedExtensions = $getFileExtension->ascSort($files);

echo '<h1>Exercício 3</h1>';

echo '<ul>';
foreach ($orderedExtensions as $extension) {
    echo "<li>{$extension}</li>";
}
echo '</ul>';