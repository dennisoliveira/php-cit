<?php
require_once __DIR__ . '/../vendor/autoload.php';

use App\Model\RecordModel;
use App\Services\Ex4\CreateRecord;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulário</title>
</head>
<body>
    <h1>Exercício 4</h1>
    <?php
        if ($_POST) {
            try {
                $createRecord = new CreateRecord();
                $record = new RecordModel();
                $record->setNome($_POST['nome']);
                $record->setSobrenome($_POST['sobrenome']);
                $record->setEmail($_POST['email']);
                $record->setTelefone($_POST['telefone']);
                $record->setLogin($_POST['login']);
                $record->setSenha($_POST['senha']);
                $createRecord->execute($record);
                echo '<p><strong>Cadastro realizado com sucesso</strong></p>';
            } catch (\Exception $erro) {
                echo "<p><strong>{$erro->getMessage()}</strong></p>";
            }
        }
    ?>
    <form action="" method="post">
        <p>
            <label for="nome">Nome</label>
            <input type="text" name="nome">
        </p>
        <p>
            <label for="sobrenome">Sobrenome</label>
            <input type="text" name="sobrenome">
        </p>
        <p>
            <label for="email">E-mail</label>
            <input type="email" name="email">
        </p>
        <p>
            <label for="telefone">Telefone</label>
            <input type="text" name="telefone">
        </p>
        <p>
            <label for="login">Login</label>
            <input type="text" name="login">
        </p>
        <p>
            <label for="senha">Senha</label>
            <input type="password" name="senha">
        </p>
        <input type="submit" value="Enviar">
    </form>
</body>
</html>
