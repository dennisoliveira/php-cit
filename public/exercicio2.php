<?php
require_once __DIR__ . '/../vendor/autoload.php';

use App\Services\Ex2\ShowWhetherWasBittenOrNot;

$showWhetherWasBittenOrNot = new ShowWhetherWasBittenOrNot();

echo '<h1>Exercício 2</h1>';

echo $showWhetherWasBittenOrNot->show("Joãozinho");
echo $showWhetherWasBittenOrNot->show("Joãozinho");
echo $showWhetherWasBittenOrNot->show("Joãozinho");
echo $showWhetherWasBittenOrNot->show("Joãozinho");