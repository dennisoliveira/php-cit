<?php
require_once __DIR__ . '/../vendor/autoload.php';

use App\Controllers\UsersController;

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $usersController = new UsersController();
        $usersController->indexAction();
        break;
    case 'POST':
        $usersController = new UsersController();
        $usersController->createAction();
        break;
    case 'PUT':
        $usersController = new UsersController();
        $usersController->updateAction();
        break;
    case 'DELETE':
        $usersController = new UsersController();
        $usersController->deleteAction();
        break;
    default:
        http_response_code(405);
}
