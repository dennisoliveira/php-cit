<?php
require_once __DIR__ . '/../vendor/autoload.php';

use App\Services\Ex1\SortCountriesByCapital;

$location = [
    'Afeganistão'   => 'Cabul',
    'África do Sul' => 'Pretória',
    'Albânia'       => 'Tirana',
    'Alemanha'      => 'Berlim',
    'Andorra'       => 'Andorra-a-Velha',
    'Angola'        => 'Luanda',
];

$sortCountriesByCapital = new SortCountriesByCapital();
$orderedArray = $sortCountriesByCapital->ascSort($location);

echo '<h1>Exercício 1</h1>';

foreach ($orderedArray as $country => $capital) {
    echo "<p>A capital do(a) <strong>{$country}</strong> é <strong>{$capital}</strong></p>";
}