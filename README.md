# Como rodar o projeto #

* php composer.phar install
* php -S localhost:8080 -t public
* Acessar: http://localhost:8080

# Exercícios #
http://localhost:8080/exercicio1.php

http://localhost:8080/exercicio2.php

http://localhost:8080/exercicio3.php

http://localhost:8080/exercicio4.php

# Exercício 7 REST API #
Listar todos usuários:

GET http://localhost:8080/exercicio7.php

Listar um usuário:

GET http://localhost:8080/exercicio7.php?id=<id_do_usuario>

Criar usuário:

POST http://localhost:8080/exercicio7.php
```json
{
    "nome": "Maria",
    "sobrenome": "da Silva",
    "email": "user1@email.com",
    "telefone": "(19) 92222-4444"
}
```

Atualizar um usuário:

PUT http://localhost:8080/exercicio7.php?id=<id_do_usuario>
```json
{
    "nome": "Maria",
    "sobrenome": "da Silva",
    "email": "user1@email.com",
    "telefone": "(19) 92222-4444"
}
```

Apagar um usuário:

DELETE http://localhost:8080/exercicio7.php?id=<id_do_usuario>