<?php
namespace App\Controllers;

use App\Model\UserModel;
use App\Repository\UsersRepository;
use App\Services\Ex7\CreateUser;
use App\Services\Ex7\DeleteUser;
use App\Services\Ex7\UpdateUser;

class UsersController
{
    public function indexAction()
    {
        $usersRepository = new UsersRepository();
        header('Content-Type: application/json');
        http_response_code(200);

        $id = $_GET['id'] ?? null;
        if ($id) {
            $user = $usersRepository->findById($id);
            if (!$user) {
                http_response_code(404);
                die;
            }
            echo $user;
            die;
        }

        $users = $usersRepository->getAll();
        $usersJson = [];
        foreach ($users as $user) {
            $usersJson[] = json_decode($user);
        }
        echo json_encode($usersJson);
    }

    public function createAction()
    {
        header('Content-Type: application/json');
        try {

            $inputJson = json_decode(file_get_contents("php://input"));
            if (
                !property_exists($inputJson, 'nome')
                || !property_exists($inputJson, 'sobrenome')
                || !property_exists($inputJson, 'email')
                || !property_exists($inputJson, 'telefone')
            ) {
                http_response_code(400);
                echo json_encode(['message' => 'Objeto JSON inválido!']);
                die;
            }

            $createUserService = new CreateUser();

            $user = new UserModel();
            $user->setNome($inputJson->nome);
            $user->setSobrenome($inputJson->sobrenome);
            $user->setEmail($inputJson->email);
            $user->setTelefone($inputJson->telefone);

            $createUserService->execute($user);

            http_response_code(201);
            echo $user;

        } catch (\Exception $erro) {
            http_response_code(400);
            echo json_encode(['message' => $erro->getMessage()]);
        }
    }

    public function updateAction()
    {
        header('Content-Type: application/json');
        $id = $_GET['id'] ?? null;
        if (!$id) {
            http_response_code(400);
            die;
        }
        try {
            $inputJson = json_decode(file_get_contents("php://input"));
            if (
                !property_exists($inputJson, 'nome')
                || !property_exists($inputJson, 'sobrenome')
                || !property_exists($inputJson, 'email')
                || !property_exists($inputJson, 'telefone')
            ) {
                http_response_code(400);
                echo json_encode(['message' => 'Objeto JSON inválido!']);
                die;
            }

            $updateUserService = new UpdateUser();

            $user = new UserModel();
            $user->setNome($inputJson->nome);
            $user->setSobrenome($inputJson->sobrenome);
            $user->setEmail($inputJson->email);
            $user->setTelefone($inputJson->telefone);

            $updatedUser = $updateUserService->execute($id, $user);

            http_response_code(200);
            echo $updatedUser;

        } catch (\Exception $erro) {
            http_response_code(400);
            echo json_encode(['message' => $erro->getMessage()]);
        }
    }

    public function deleteAction()
    {
        try {
            header('Content-Type: application/json');
            $id = $_GET['id'] ?? null;
            if (!$id) {
                http_response_code(400);
                die;
            }

            $deleteUserService = new DeleteUser();
            $user = $deleteUserService->execute($id);

            http_response_code(200);
            echo $user;
        } catch (\Exception $erro) {
            http_response_code(404);
            echo json_encode(['message' => $erro->getMessage()]);
        }
    }
}