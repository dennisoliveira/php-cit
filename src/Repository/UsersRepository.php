<?php

namespace App\Repository;

use App\Model\UserModel;

class UsersRepository
{
    protected static $users = [];

    public function __construct()
    {
        if(file_exists(__DIR__ . '/../../data/usuarios.txt')) {
            $this->recover();
        } else {
            $this->persist();
        }
    }

    protected function persist(): void
    {
        $serializedUsers = serialize(self::$users);
        file_put_contents(__DIR__ . '/../../data/usuarios.txt', $serializedUsers);
    }

    protected function recover(): void
    {
        $recoveredData = file_get_contents(__DIR__ . '/../../data/usuarios.txt');
        $recoveredArray = unserialize($recoveredData);
        self::$users = $recoveredArray;
    }

    public function create(UserModel $user): string {
        self::$users[$user->getId()] = $user;
        $this->persist();

        end(self::$users);
        return key(self::$users);
    }

    public function update(UserModel $user): ?UserModel {
        self::$users[$user->getId()] = $user;
        $this->persist();
        return $user;
    }

    public function getAll(): array
    {
        return self::$users;
    }

    public function findById(string $id): ?UserModel
    {
        $findUser = null;
        foreach (self::$users as $user) {
            if ($user->getId() == $id) {
                $findUser = $user;
                break;
            }
        }
        return $findUser;
    }

    public function findByEmail(string $email): ?UserModel
    {
        $findUser = null;
        foreach (self::$users as $user) {
            if ($user->getEmail() == $email) {
                $findUser = $user;
                break;
            }
        }
        return $findUser;
    }

    public function deleteById(string $id): void
    {
        unset(self::$users[$id]);
        $this->persist();
    }
}