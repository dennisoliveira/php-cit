<?php

namespace App\Repository;

use App\Model\RecordModel;

class RecordsRepository
{
    protected static $records = [];

    public function __construct()
    {
        if(file_exists(__DIR__ . '/../../data/registros.txt')) {
            $this->recover();
        } else {
            $this->persist();
        }
    }

    protected function persist(): void
    {
        $serializedRecords = serialize(self::$records);
        file_put_contents(__DIR__ . '/../../data/registros.txt', $serializedRecords);
    }

    protected function recover(): void
    {
        $recoveredData = file_get_contents(__DIR__ . '/../../data/registros.txt');
        $recoveredArray = unserialize($recoveredData);
        self::$records = $recoveredArray;
    }

    public function create(RecordModel $record): int {
        self::$records[] = $record;
        $this->persist();

        end(self::$records);
        return key(self::$records);
    }

    public function getAll(): array
    {
        return self::$records;
    }

    public function findByEmail(string $email)
    {
        $findRecord = null;
        foreach (self::$records as $record) {
            if ($record->getEmail() == $email) {
                $findRecord = $record;
                break;
            }
        }
        return $findRecord;
    }

    public function findByLogin(string $login)
    {
        $findRecord = null;
        foreach (self::$records as $record) {
            if ($record->getLogin() == $login) {
                $findRecord = $record;
                break;
            }
        }
        return $findRecord;
    }
}