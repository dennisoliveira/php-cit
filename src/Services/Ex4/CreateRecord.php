<?php

namespace App\Services\Ex4;

use App\Repository\RecordsRepository;
use App\Model\RecordModel;

class CreateRecord
{
    protected $recordsRepository;

    public function __construct()
    {
        $this->recordsRepository = new RecordsRepository();
    }

    public function execute(RecordModel $record): int
    {

        if ($this->recordsRepository->findByEmail($record->getEmail())) {
            throw new \Exception('O email já existe');
        }

        if ($this->recordsRepository->findByLogin($record->getLogin())) {
            throw new \Exception('O login já existe');
        }

        return $this->recordsRepository->create($record);
    }
}