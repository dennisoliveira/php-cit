<?php

namespace App\Services\Ex7;

use App\Repository\UsersRepository;
use App\Model\UserModel;

class UpdateUser
{
    protected $usersRepository;

    public function __construct()
    {
        $this->usersRepository = new UsersRepository();
    }

    public function execute(string $id, UserModel $newUser): UserModel
    {
        $user = $this->usersRepository->findById($id);
        if (!$user) throw new \Exception('Não foi possível localizar o usuário');

        $user->setNome($newUser->getNome());
        $user->setSobrenome($newUser->getSobrenome());
        $user->setEmail($newUser->getEmail());
        $user->setTelefone($newUser->getTelefone());

        $this->usersRepository->update($user);
        return $user;
    }
}