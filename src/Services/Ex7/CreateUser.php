<?php

namespace App\Services\Ex7;

use App\Repository\UsersRepository;
use App\Model\UserModel;

class CreateUser
{
    protected $usersRepository;

    public function __construct()
    {
        $this->usersRepository = new UsersRepository();
    }

    public function execute(UserModel $user): string
    {
        if ($this->usersRepository->findByEmail($user->getEmail())) {
            throw new \Exception('O email já existe');
        }
        return $this->usersRepository->create($user);
    }
}