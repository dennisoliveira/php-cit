<?php

namespace App\Services\Ex7;

use App\Repository\UsersRepository;
use App\Model\UserModel;

class DeleteUser
{
    protected $usersRepository;

    public function __construct()
    {
        $this->usersRepository = new UsersRepository();
    }

    public function execute(string $id): UserModel
    {

        $user = $this->usersRepository->findById($id);
        if (!$user) throw new \Exception('Não foi possível localizar o usuário');

        $this->usersRepository->deleteById($id);
        return $user;
    }
}