<?php

namespace App\Services\Ex3;


class GetFileExtension
{
    protected function getFileExtension(string $filename)
    {
        return substr($filename, strpos($filename, '.'));
    }

    public function ascSort(array $filenames)
    {
        $orderedExtensions = [];
        foreach ($filenames as $filename) {
            $fileExtension = $this->getFileExtension($filename);
            $orderedExtensions[] = $fileExtension;
        }
        asort($orderedExtensions);
        return $orderedExtensions;
    }
}