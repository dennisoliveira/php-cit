<?php

namespace App\Services\Ex2;


class ShowWhetherWasBittenOrNot
{
    protected static $bitten;

    protected function randomizeBites(): bool
    {
        if (self::$bitten === null) {
            self::$bitten = boolval(rand(0, 1));
            return self::$bitten;
        }
        self::$bitten = !self::$bitten;
        return self::$bitten;
    }

    public function show(string $people): string
    {
        if ($this->randomizeBites()) return "<p>{$people} <strong>mordeu</strong> o seu dedo!</p>";
        return "<p>{$people} <strong>NÂO</strong> mordeu o seu dedo!</p>";
    }
}