<?php

namespace App\Services\Ex1;


class SortCountriesByCapital
{
    public function ascSort(array $locationArray): array
    {
        asort($locationArray);
        return $locationArray;
    }
}