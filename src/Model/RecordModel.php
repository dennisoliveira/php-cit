<?php

namespace App\Model;

class RecordModel
{
    protected $nome = "";
    protected $sobrenome = "";
    protected $email = "";
    protected $telefone = "";
    protected $login = "";
    protected $senha = "";

    protected function emailIsValid(string $email): bool
    {
        return !filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function phoneIsValid(string $phone): bool
    {
        return !preg_match('/^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/', $phone);
    }

    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setSobrenome(string $sobrenome): void
    {
        $this->sobrenome = $sobrenome;
    }

    public function getSobrenome(): string
    {
        return $this->sobrenome;
    }

    public function setEmail(string $email): void
    {
        if ($this->emailIsValid($email)) throw new \Exception('email não é válido');
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setTelefone(string $telefone): void
    {
        if ($this->phoneIsValid($telefone)) throw new \Exception('telefone não é válido');
        $this->telefone = $telefone;
    }

    public function getTelefone(): string
    {
        return $this->telefone;
    }

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setSenha(string $senha): void
    {
        $this->senha = password_hash($senha, PASSWORD_BCRYPT);
    }

    public function getSenha(): string
    {
        return $this->senha;
    }

}